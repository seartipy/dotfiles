This repository consists of configuration for zsh, bash, visual studio code, emacs, atom, clojure, scala, haskell, python and web development.

### Installation

#### Automated install

Automated installer script will work only with the latest Ubuntu and Mac operating systems.

##### Linux

1. **Clojure**

        wget -qO- bit.ly/srtpdtf > ~/setup && bash ~/setup clojure

2. **Scala**

        wget -qO- bit.ly/srtpdtf > ~/setup && bash ~/setup scala

3. **Web**

        wget -qO- bit.ly/srtpdtf > ~/setup && bash ~/setup web vscode

4. **Everything**

        wget -qO- bit.ly/srtpdtf > ~/setup && bash ~/setup everything

##### Mac

1. **Clojure**

        curl -L bit.ly/srtpdtf > ~/setup && bash ~/setup clojure

2. **Scala**

        curl -L bit.ly/srtpdtf > ~/setup && bash ~/setup scala

3. **Web**

        curl -L bit.ly/srtpdtf > ~/setup && bash ~/setup web vscode

4. **Everything**

        curl -L bit.ly/srtpdtf > ~/setup && bash ~/setup

**Please Note** :

* No files will be deleted by the installer. All your current files will be backed up to ~/seartipy.backups directory.

* If ~/seartipy.backups already exists, it will be moved to trash.
